# Contributor: Leo <thinkabit.ukim@gmail.com>
# Maintainer:
pkgname=py3-validators
pkgver=0.20.0
pkgrel=2
pkgdesc="Python3 Data Validation for Humans"
url="https://github.com/python-validators/validators"
arch="noarch"
license="MIT"
depends="py3-decorator py3-six"
makedepends="py3-gpep517 py3-setuptools py3-wheel"
checkdepends="py3-isort py3-pytest py3-flake8"
subpackages="$pkgname-pyc"
source="https://github.com/python-validators/validators/archive/$pkgver/py3-validators-$pkgver.tar.gz"
builddir="$srcdir/validators-$pkgver"

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	.testenv/bin/python3 -m pytest
}

package() {
	python3 -m installer -d "$pkgdir" .dist/*.whl
}

sha512sums="
45a07c061022da453c31fc946950be3a701f36afbf0e0eb82b91f4bdb26c4d2d0ab47f6958ac88fd2e0a1563201e946014a5cb93cfa1c6710411982852d571c7  py3-validators-0.20.0.tar.gz
"
