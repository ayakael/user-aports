#!/bin/bash

# we chroot into codex root, thus need to bind mount
mount -o bind /dev /usr/lib/xochitl-bin/dev
mount -o bind /proc /usr/lib/xochitl-bin/proc
mount -o bind /sys /usr/lib/xochitl-bin/sys

# xochitl expects home to be mounted
mount -o bind /var/lib/xochitl-bin /usr/lib/xochitl-bin/home/root

# start xochitl
chroot /usr/lib/xochitl-bin /usr/bin/xochitl
