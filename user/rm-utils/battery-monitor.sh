#!/usr/bin/env bash
#
# battery-monitor.sh
# Prints the state of charge of the tablet's battery
#
# Parabola-rM is a free operating system for the reMarakble tablet.
# Copyright (C) 2020
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.

# Path for Linux 4.9
battpath="/sys/class/power_supply/bq27441-0"

chargenow="$(cat $battpath/charge_now)"
chargefull="$(cat $battpath/charge_full)"
status="$(cat $battpath/status)"

chargepct="$(echo $chargenow $chargefull \
	| awk '{printf "%f", $1 / $2 * 100}' \
	| cut -d'.' -f1)"
symbol=""
if [[ "Charging" == "$status" ]]; then
	symbol=$'\u26a1' # Lightning symbol
fi

echo "${symbol}${chargepct}%"
