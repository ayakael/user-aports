#!/bin/sh

BUNDLE_DIR='/usr/share/webapps/recipes'

cd $BUNDLE_DIR

if [ "$(id -un)" != 'recipes' ]; then
	exec su recipes -c '"$0" "$@"' -- ./manage.py "$@"
else
	exec ./manage.py "$@"
fi
