# ayports
Upstream: https://codeberg.org/ayakael/ayports

## Description

This repository contains aports that are not yet merged in the official Alpine
Linux repository or don’t adhere to Alpine polices. Packages are automatically
built using CI. Once built, they are deployed to a git-lfs repository, making
them available to apk.

Branches are matched to Alpine releases.

## Repositories

You can browse all the repositories at https://codeberg.org/ayakael/ayports
Affixed to each repository description is the appropriate link for use in
`/etc/apk/repositories`.

#### Backports
```
https://ayakael.net/pkgs/apk/media/branch/edge/backports
```

Aports from the official Alpine repositories backported from edge.

#### User
```
https://ayakael.net/pkgs/apk/media/branch/edge/user
```

Aports that have yet to be (or may never be) upstreamed to the official
aports.


## How to use
Add security key of the apk repository to your /etc/apk/keys:

```shell
cd /etc/apk/keys
wget https://ayakael.net/pkgs/apk/raw/branch/edge/antoine.martin@protonmail.com-5b3109ad.rsa.pub
```
Add repositories that you want to use (see above) to `/etc/apk/repositories`.

## Support
Generally, only the latest branch is kept up-to-date. That said, if an aport
is broken on the latest release due to a dependency incompatibility, it will be
kept up-to-date on the release it last works on.

As these aports are built for my own application, I make no guarantees that
they will work for you.

## Contribution & bug reports
If you wish to contribute to this aports collection, or wish to report a bug,
you can do so on Codeberg here:
https://codeberg.org/ayakael/ayports/issues

For packages that are in backports, bug reports and merge requests
should be done on Alpine's aports repo instance:
https://gitlab.alpinelinux.org/alpine/aports

## License
This readme, abuilds and support scripts are licensed under MIT License.
