#!/bin/sh

# shellcheck disable=SC3043

. $CI_PROJECT_DIR/.gitlab/bin/functions.sh

# shellcheck disable=SC3040
set -eu -o pipefail

readonly APORTSDIR=$CI_PROJECT_DIR
readonly REPOS="backports user"
readonly BASEBRANCH=$CI_MERGE_REQUEST_TARGET_BRANCH_NAME

export GIT_SSH_COMMAND="ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no"

gitlab_key_to_rsa $ABUILD_KEY rsa-private $HOME/.abuild/$ABUILD_KEY_NAME.rsa
gitlab_key_to_rsa $ABUILD_KEY_PUB rsa-public $HOME/.abuild/$ABUILD_KEY_NAME.rsa.pub
gitlab_key_to_rsa $SSH_KEY rsa-private $HOME/.ssh/id_rsa
chmod 700 "$HOME"/.ssh/id_rsa
chmod 700 "$HOME"/.abuild/$ABUILD_KEY_NAME.rsa

echo "PACKAGER_PRIVKEY=$HOME/.abuild/$ABUILD_KEY_NAME.rsa" > $HOME/.abuild/abuild.conf
echo "REPODEST=$HOME/repo-apk" >> $HOME/.abuild/abuild.conf
sudo cp $HOME/.abuild/$ABUILD_KEY_NAME.rsa.pub /etc/apk/keys/.

if [ -d $HOME/repo-apk ]; then
	git -C $HOME/repo-apk fetch
	git -C $HOME/repo-apk checkout $BASEBRANCH
	git -C $HOME/repo-apk pull --rebase
else
	git clone forgejo@ayakael.net:forge/repo-apk.git -b $BASEBRANCH $HOME/repo-apk
fi

git -C $HOME/repo-apk annex sync --content

for i in $(find packages -type f -name "*.apk"); do
	install -vDm644 $i ${i/packages/$HOME\/repo-apk}
done

fetch_flags="-qn"
git fetch $fetch_flags "$CI_MERGE_REQUEST_PROJECT_URL" \
	"+refs/heads/$BASEBRANCH:refs/heads/$BASEBRANCH"

for repo in $(changed_repos); do
	rm $HOME/repo-apk/$repo/*/APKINDEX.tar.gz | true
	mkdir -p $repo/DUMMY
	echo "pkgname=DUMMY" > $repo/DUMMY/APKBUILD
	cd $repo/DUMMY
	for i in $(find $HOME/repo-apk/$repo -maxdepth 1 -mindepth 1 -printf '%P '); do
		CHOST=$i abuild index
	done
	cd "$CI_PROJECT_DIR"
	rm -R $repo/DUMMY
done

git -C $HOME/repo-apk add .
git -C $HOME/repo-apk commit -m "Update from $CI_MERGE_REQUEST_IID - $CI_MERGE_REQUEST_TITLE"
git -C $HOME/repo-apk annex sync --content
